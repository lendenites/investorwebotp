package steps;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Login
{
	WebDriver driver;
	String Url="https://app.lendenclub.com/signin";//for sign in
	
	//for url page
	String URL="https://ldc.lendenclub.com/question#eyJkYXRhc2V0X3F1ZXJ5Ijp7InR5cGUiOiJxdWVyeSIsInF1ZXJ5Ijp7InNvdXJjZS10YWJsZSI6MzMsImZpbHRlciI6WyJhbmQiLFsiPSIsWyJmaWVsZC1pZCIsMjE5NV0sIjg0MzM1MDg4OTEiXV19LCJkYXRhYmFzZSI6Mn0sImRpc3BsYXkiOiJ0YWJsZSIsInZpc3VhbGl6YXRpb25fc2V0dGluZ3MiOnt9fQ==";
	public Login(WebDriver driver) throws InterruptedException
	{
		this.driver=driver;
		driver.get(Url);
		driver.findElement(By.xpath("//input[@name='mobile_number']")).sendKeys("8433508891");
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/div[2]/div[2]/span[2]/span")).click();
		Thread.sleep(5000);
		((JavascriptExecutor)driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get("https://ldc.lendenclub.com/auth/login?redirect=%2F");
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		driver.findElement(By.name("username")).sendKeys("saurabh.s@lendenclub.com");
		driver.findElement(By.name("password")).sendKeys("saurabh123");
		driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(2000);
		driver.get(URL);
		Thread.sleep(10000);
		String text=driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div/div[3]/div")).getText();
		System.out.println("OTP is :"+text);
		driver.switchTo().window(tabs.get(0));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='otp']")).sendKeys(text);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[2]/div/div[4]/button")).click();
	}

}
